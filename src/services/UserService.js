const DatabaseService = require('@services/DatabaseService');
const axios = require('axios');

class User {
    constructor() {
        this._db = DatabaseService.db;
    }

    async getTeachers(category_id) {
        let res;
        try {
            const subquery = this._db('teachers')
                .innerJoin('users', 'users.user_id', 'teachers.user_id')
                .innerJoin('courses', 'users.user_id', 'courses.user_id')
                .innerJoin('categories', 'courses.category_id', 'categories.category_id')
                .distinct('users.user_id')
                .select()
                .where('courses.category_id', category_id);
            res = await this._db('users')
                .innerJoin('teachers', 'users.user_id', 'teachers.user_id')
                .where('users.user_id', 'in', subquery);
        } catch (err) {
            console.log(err);
        }
        return res;
    }

    async getUser(user_id) {
        let res;
        try {
            res = this._db('users')
                .select()
                .where('user_id', user_id);
        } catch (err) {
            console.log(err);
        }
        return res;
    }

    async getCourses(teacher_id) {
        let res;
        try {
            res = await this._db('courses')
                .select()
                .where({user_id: teacher_id});
        } catch (err) {
            console.log(err);
        }
        return res;
    }

    async login(user) {
        const res = await this._db.count().where({
            email: user.email,
            password: user.password
        });
        return Number.parseInt(res[0].count) ? true : false;
    }

    async getAvatar() {
        const img = await axios.get('https://randomuser.me/api/');
        return img.data.results[0].picture.large;
    }
}

module.exports = User;
