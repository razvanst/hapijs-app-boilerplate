const path = require('path');
const fs = require('fs');

const directoryPath = path.join(__dirname);

module.exports = new Promise((resolve, reject) => {
    fs.readdir(directoryPath, function (err, files) {
        let routes = [];
        if (err) {
            return console.log('Unable to load routes');
        }

        files.forEach(function (file) {
            if (file === 'Router.js') return;
            routes.push(require('@routes/' + file));
        });
        console.log(routes);
        resolve(routes);
    });
});

